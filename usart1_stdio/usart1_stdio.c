#include "usart1_stdio.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>

static char usart1ScanfBuf[32] = {0};
static uint8_t usart1ScanfOffset = 0;

//printf功能实现
int fputc(int ch, FILE *f)
{ 
  HAL_UART_Transmit(&huart1, (uint8_t*)&ch, 1, 1000);
  return(ch);
}

//scanf功能实现
int fgetc(FILE *f)
{
  uint8_t data;
  HAL_StatusTypeDef sta;
  
  while(1)
  {
    if( strlen(usart1ScanfBuf) > 0 ) 
    {
      if( strstr(usart1ScanfBuf, "\r")  != NULL ) 
      {
        data = usart1ScanfBuf[usart1ScanfOffset++];
        if( data == '\r' ) 
        {
          memset(usart1ScanfBuf, 0, sizeof(usart1ScanfBuf));
          usart1ScanfOffset = 0;
        }
        break;
      }
    }
    
    sta = HAL_UART_Receive(&huart1, &data, 1, 10);
    if( sta == HAL_OK ) break;
  }
  
  return data;
}

//向scanf写入数据，跳出scanf阻塞, 写入的数据长度应小于32字节
void USART1_SetScanfBuf(char *data)
{
  usart1ScanfOffset = 0;
  if( strstr(data, "\r")  == NULL ) {
    snprintf(usart1ScanfBuf, sizeof(usart1ScanfBuf), "%s\r", data);
  } else {
    snprintf(usart1ScanfBuf, sizeof(usart1ScanfBuf), "%s", data);
  }
}
