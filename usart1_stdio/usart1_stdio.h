#ifndef USART1_STDIO_H
#define USART1_STDIO_H
#ifdef __cplusplus
 extern "C" {
#endif

#include "usart.h"

void USART1_SetScanfBuf(char *data);

#ifdef __cplusplus
}
#endif
#endif 
