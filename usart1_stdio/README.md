前置：
1. 在STM32CubeMX中打开USART1，配置项目均保持默认即可

描述：
1. USART1支持printf与scanf功能

特殊：
1. 在使用scanf功能时，可通过USART1_SetScanfBuf向scanf写入数据，跳出scanf阻塞